package services.main;

import com.google.cloud.datastore.*;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class DBOperations {

    private static final Datastore datastore = DatastoreOptions.getDefaultInstance().getService();
    private static final KeyFactory userProductsKeyFactory = datastore.newKeyFactory().setKind("UserProducts");
    private static final KeyFactory recipesMatchKeyFactory = datastore.newKeyFactory().setKind("RecipesMatch");

    public static void doSomething(){
        Query<Entity> query = Query.newEntityQueryBuilder()
                .setKind("RecipesIntents")
                .build();

        QueryResults<Entity> recipes = datastore.run(query);

        while(recipes.hasNext()){
            Entity entity = recipes.next();
            System.out.println("stack: " + entity.getString("queues"));

            ArrayList<Stack> list = services.main.StackOperations.getStack(entity.getString("queues"));

            if(verifyIsComplete(list)){
                System.out.println("List is complete: " + list);
                List<String> usersList = removeUsers(list);
                System.out.println("List of users: " + usersList);
                Entity task = Entity.newBuilder(datastore.get(entity.getKey())).set("queues",
                        services.main.StackOperations.getString(list)).build();
                datastore.update(task);
                moveToMatch(usersList, entity.getString("recipeId"));
            }


        }

    }

    private static boolean verifyIsComplete(ArrayList<Stack> list){
        for(Stack stack: list)
            if(!stack.containsElems())
                return  false;
        return true;
    }

    private static List<String> removeUsers(ArrayList<Stack> list){
        List<String> userList = new ArrayList<String>();

        for(Stack stack: list){
            String usr = stack.removeUserId();
            if(!userList.contains(usr))
                userList.add(usr);
            removeItem(usr, stack.ingredient);
        }

        return userList;
    }

    private static void removeItem(String user, String item){
        Query<Entity> query = Query.newEntityQueryBuilder()
                .setKind("UserProducts")
                .setFilter(StructuredQuery.CompositeFilter.and(
                        StructuredQuery.PropertyFilter.eq("productId", item),
                        StructuredQuery.PropertyFilter.eq("userId", user)))
                .build();

        QueryResults<Entity> items = datastore.run(query);

        while(items.hasNext()){
            Entity entity = items.next();

            System.out.println("NOT OKAY: " + entity.getString("productId") + " " + entity.getString("quantity"));

            int quantity = Integer.parseInt(entity.getString("quantity"));
            if(quantity == 1){
                datastore.delete(userProductsKeyFactory.newKey(entity.getKey().getId()));
            }
            else{
                Entity task = Entity.newBuilder(datastore.get(entity.getKey())).set("quantity", String.valueOf(quantity - 1)).build();
                datastore.update(task);
            }
        }

    }

    private static void moveToMatch(List<String> users, String recipe){
        String usersConcat = "";
        System.out.println("Move to match users: " + users);
        for(String usr: users)
            usersConcat += usr + ";";
        usersConcat = usersConcat.substring(0, usersConcat.length() - 1);

        Key key = datastore.allocateId(recipesMatchKeyFactory.newKey());
        Entity entity = Entity.newBuilder(key)
                .set("RecipeId", recipe)
                .set("Users", usersConcat)
                .build();
        datastore.put(entity);

    }

}
