package services.main;

import java.util.ArrayList;

public class Stack {

    String ingredient;
    ArrayList<String> userId;

    public String getIngredient() {
        return ingredient;
    }

    public void setIngredient(String ingredient) {
        this.ingredient = ingredient;
    }

    public ArrayList<String> getUserId() {
        return userId;
    }

    public void setUserId(ArrayList<String> userId) {
        this.userId = userId;
    }

    public void addUser(String userId){
        this.userId.add(userId);
    }
    public String removeUserId(){
        return this.userId.remove(0);
    }
    public boolean containsElems(){
        return !this.userId.isEmpty();
    }
}
