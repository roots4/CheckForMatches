package services.main;

import services.main.Stack;

import java.util.ArrayList;

public class StackOperations {

    public static ArrayList<Stack> getStack(String input) {
        if(input == null)
            return null;


        String[] parts1 = input.split(";");

        ArrayList<Stack> stacks = new ArrayList<Stack>();

        for(int i = 0; i < parts1.length; i++) {
            Stack stack = new Stack();

            String part1 = parts1[i];
            String[] parts2 = part1.split("-");

            stack.setIngredient(parts2[0]);
            if(parts2.length > 1) {
                String part2 = parts2[1];

                String[] parts3 = part2.split(",");
                ArrayList<String> ing = new ArrayList<String>();
                for (int j = 0; j < parts3.length; j++) {
                    String part3 = parts3[j];
                    ing.add(part3);
                }
                stack.setUserId(ing);
            } else {
                stack.setUserId(new ArrayList<String>());
            }

            stacks.add(stack);
        }

        return stacks;

    }

    public static String getString(ArrayList<Stack> stacks){
        String output = "";

        for(Stack stack : stacks) {
            output += stack.getIngredient() + "-";
            ArrayList<String> userId = stack.getUserId();
            if(userId.isEmpty()){
                output += ",";
            }
            for(String user : userId) {
                output += user + ",";
            }
            output = output.substring(0, output.length()-1);
            output += ";";
        }
        output = output.substring(0, output.length()-1);
        return output;
    }

}
